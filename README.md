# Choice in CoDesign

A Haskell module implementing definitions from the "Choice in CoDesign" Report by Marius Furter (available in git repository proj-sp-furter/report and at https://www.research-collection.ethz.ch/handle/20.500.11850/532280).

This module was written by Prof. Alberto Cattaneo and was annotated by Marius Furter. 
